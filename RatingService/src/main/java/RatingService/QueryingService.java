package RatingService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

public class QueryingService
{
    
    private String getPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException{
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for(Map.Entry<String, String> entry : params.entrySet()){
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }
    
    private String performCall(String requestURL,
            HashMap<String, String> DataParams) throws IOException{

        URL url;
        url = new URL(requestURL + getPostDataString(DataParams));

        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        
        int responseCode = conn.getResponseCode();
        StringBuffer response = new StringBuffer();
        if (responseCode == HttpURLConnection.HTTP_OK) { 
            BufferedReader in = new BufferedReader(new InputStreamReader(
                    conn.getInputStream()));
            String inputLine;
 
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();
 
        }
        return response.toString();
    }
    
    public HashMap<String, String> getTop25CupsEuropeWOTParameters()
    {
        HashMap<String, String> DataParams = new HashMap<>();
        DataParams.put("types", "cup");
        DataParams.put("states", "finished");
        DataParams.put("limit.total", "25");
        DataParams.put("path", "/play/worldoftanks/europe/");
        
        return DataParams;
    }
    
    public HashMap<String, String> getTop25RatingParameters()
    {
        HashMap<String, String> DataParams = new HashMap<>();
        DataParams.put("limit", "25");
        
        return DataParams;
    }
     
    public String queryESLCups(HashMap<String, String> DataParams) {
        try
        {
          String ESL_URL = "http://play.eslgaming.com/api/leagues?";
          return performCall(ESL_URL, DataParams);
        }
        catch(Exception e)
        {
            return "";
        }
    }
    
    public String queryCup(int id, HashMap<String, String> DataParams) {
        try
        {
          String ESL_URL = "http://play.eslgaming.com/api/leagues/";
          String ranking = "/ranking?";
          return performCall(ESL_URL + Integer.toString(id) + ranking, DataParams);
        }
        catch(Exception e)
        {
            return "";
        }
    }
  
}
