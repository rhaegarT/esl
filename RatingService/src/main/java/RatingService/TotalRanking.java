package RatingService;

import java.util.HashMap;
import java.util.Map;


public class TotalRanking {
    
    private Map<Integer, GroupRanking> teams;
    
    public TotalRanking()
    {
        this.teams = new HashMap<>(); 
    }
    
	public GroupRanking getBySize(int size)
	{
	    return teams.get(size);
	}
	
    public void updateRanking(int groupSize, rank[] rankingInCup)
    {
        GroupRanking group;
        if(teams.containsKey(groupSize))
        {
           group = teams.get(groupSize);
           group.updateRanking(rankingInCup);
        }
        else
        {
           group = new GroupRanking(groupSize);
           group.updateRanking(rankingInCup);
           teams.put(groupSize, group);
        }
    }
} 

