package RatingService;

import java.util.ArrayList;

public class ESLCupsStats
{
    class cupName
    {
        String full;
        String normal;
        String small;
    }
    
    class Restriction
    {
        class Parameters
        {
            public String type;
            public String minmembers;
        }
        public String type;
        public Parameters params;
        
    }
    
    class timestamp
    {
      public String begin;
      public String end;
    }
    
    class time
    {
        public timestamp signUp;
        public timestamp inProgress;
        public timestamp finished;
        public timestamp checkIn;
        public timestamp postSignUp;
    }
    
    class reg
    {
        public boolean verificationRequired;
        public boolean premiumRequired;
    }
    
    class contest
    {
        public int signedUp;
        public int checkedIn;
        public int max;
    }
    
    public int id;
    //--------Complete deserialization for future--------------------------//
/*  public String type;
    public cupName name;
    public String uri;
    public String mode;
    public String resultType;*/
    public int teamSize;
/*  public String skillLevel;
    public String series;
    public String prizePool;
    public String gameAccountType;
    public String gameIntegration;
    public boolean matchSetupAllowed;
    public boolean anticheatEnable;
    public int gameId;
    public ArrayList<Restriction> restrictions;
    public time timeline;
    public reg signUp;
    public contest contestants;
    public String state;*/
}