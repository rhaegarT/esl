package RatingService;

import java.util.HashMap;
import java.util.Map;

 class TeamRating
{
      public int cupPlayed;
      public int highestPosition;
      public int lowestPosition;
      TeamRating()
      {
        this.cupPlayed = 0;
        this.highestPosition = 9999;
        this.lowestPosition = 0;
      }
}
	
public class GroupRanking
{
    
    int teamSize;
    private Map<String, TeamRating> teams;
    
    private void updateTeam(TeamRating rating, rank rateInCup)
    {
        rating.cupPlayed += 1;
        rating.highestPosition = Math.min(rating.highestPosition, rateInCup.position);
        rating.lowestPosition = Math.max(rating.lowestPosition, rateInCup.position);
    }
    
    private void createTeam(String teamName, rank rateInCup)
    {
        TeamRating rating = new TeamRating();        
        rating.cupPlayed = 1;
        rating.highestPosition = rateInCup.position;
        rating.lowestPosition = rateInCup.position;
        
        teams.put(teamName, rating);
    }
    
    public GroupRanking(int teamSize)
    {
       this.teams = new HashMap<>(); 
       this.teamSize = teamSize;
    }
    
	public TeamRating getByName(String name)
	{
	    return teams.get(name);
	}
	
    public void updateRanking(rank[] rankingInCup)
    {
        for(rank teamRank: rankingInCup)
        {
            String teamName = teamRank.team.name;
            if(teamRank.team.suffix != null)
             if(!teamRank.team.suffix.isEmpty())
               teamName += "." + teamRank.team.suffix;

			TeamRating rating = teams.get(teamName);
            if(rating != null)
              updateTeam(rating, teamRank);
            else
              createTeam(teamName, teamRank);
        }
    }
    
}
