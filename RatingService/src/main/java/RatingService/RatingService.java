package RatingService;


import java.io.OutputStream;

import java.io.IOException;


import java.util.HashMap;
import java.util.Map;

import java.net.InetSocketAddress;
import java.lang.reflect.Type;

import com.google.gson.Gson;

import com.google.gson.reflect.TypeToken;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;





class RequestHandler implements HttpHandler 
{
    @Override
    public void handle(HttpExchange t) throws IOException {
      TotalRanking TeamsRanking = new TotalRanking();
      QueryingService queryObj = new QueryingService();
      String cups = queryObj.queryESLCups(queryObj.getTop25CupsEuropeWOTParameters());
      
      Gson g = new Gson();
      try
      {
        Type EslCupStatType = new TypeToken<Map<String, ESLCupsStats>>(){}.getType();  
        Map<String, ESLCupsStats> statistics = g.fromJson(cups, EslCupStatType);
        
        for (Map.Entry<String,ESLCupsStats> entry : statistics.entrySet()) {
            ESLCupsStats value = entry.getValue();
            int teamSize = value.teamSize;
            String cupResult = queryObj.queryCup(value.id, queryObj.getTop25RatingParameters());
            
            ESLTeamRanking cupResultStatistics = g.fromJson(cupResult, ESLTeamRanking.class);
            if(cupResultStatistics!=null && cupResultStatistics.ranking!=null)
              TeamsRanking.updateRanking(teamSize, cupResultStatistics.ranking);
        }
        String result = g.toJson(TeamsRanking);
        byte[] data = result.getBytes("UTF-8");
        t.sendResponseHeaders(200, data.length);
        
        OutputStream os = t.getResponseBody();
        os.write(data);
        os.close();
      }
      catch(Exception e)
      {
          System.out.println(e.toString());
      }
    }

}

public class RatingService {
  
    public static void main(String[] args) {
      try
      {
        HttpServer server = HttpServer.create(new InetSocketAddress(8000), 0);
        server.createContext("/ranking", new RequestHandler());
        server.setExecutor(null); 
        server.start();
      }catch(Exception e)
      {
          
      }
    }
    
}
