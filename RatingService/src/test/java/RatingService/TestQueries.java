package RatingService;

import java.util.Map;
import java.util.HashMap;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TestQueries
{
  @Test
  public void testTop25CupsEuropeWOTParameters() {

    QueryingService tester = new QueryingService();

    Map<String, String> result =  tester.getTop25CupsEuropeWOTParameters();
    
    assertEquals("Should be cups", "cup" , result.get("types"));
    assertEquals("Should be finished ones", "finished" , result.get("states"));
    assertEquals("No more than 25", "25" , result.get("limit.total"));
    assertEquals("WorldOfTanks:Europe", "/play/worldoftanks/europe/" , result.get("path"));
  }
  
  @Test
  public void testTop25Rating() {

    QueryingService tester = new QueryingService();

    Map<String, String> result =  tester.getTop25RatingParameters();
    
    assertEquals("No more than 25", "25" , result.get("limit"));
  }
  
  @Test
  public void testQuery() {

    QueryingService tester = new QueryingService();

	HashMap<String, String> DataParams = new HashMap<>();
    DataParams.put("types", "cup");
    DataParams.put("path", "/play/worldoftanks/europe/");
    String result =  tester.queryESLCups(DataParams);
    
    assertTrue(!result.isEmpty());
  }
  
  @Test
  public void testQueryByID() {

    QueryingService tester = new QueryingService();

	int id = 111236;
	HashMap<String, String> DataParams = new HashMap<>();
    String result =  tester.queryCup(id, DataParams);
	
	assertTrue(!result.isEmpty());
  }
}
