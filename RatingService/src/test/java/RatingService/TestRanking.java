package RatingService;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


import org.junit.Test;

public class TestRanking
{
  @Test
  public void testRankingSystem() {

    TotalRanking tester = new TotalRanking();
	
    rank r1 = new rank();
	r1.position = 1;
	r1.team.name = "testTeam1";
	r1.team.suffix = "VP";
	
	rank r2 = new rank();
	r2.position = 2;
	r2.team.name = "testTeam2";
	r2.team.suffix = "EG";
	
	rank r3 = new rank();
	r3.position = 3;
	r3.team.name = "testTeam1";
    r3.team.suffix = "VP";
	
	rank r4 = new rank();
	r4.position = 10;
	r4.team.name = "testTeam1";
	
	int teamSize = 7;
	rank[] testRanks = {r1, r2, r3, r4};
	
	tester.updateRanking(teamSize, testRanks);
	GroupRanking SevenSizeGroup = tester.getBySize(teamSize);
	
    assertTrue(SevenSizeGroup != null);
	
	TeamRating testing_r1 = SevenSizeGroup.getByName("testTeam1.VP");
	assertEquals("Set first place on creation", 1 , testing_r1.highestPosition);
	assertEquals("Set third place on update", 3 , testing_r1.lowestPosition);
	assertEquals("Both cups are evaluated", 2 , testing_r1.cupPlayed);

	TeamRating testing_r2 = SevenSizeGroup.getByName("testTeam2.EG");
	assertEquals("Set first place on creation", 2 , testing_r2.highestPosition);
	assertEquals("Set third place on update", 2 , testing_r2.lowestPosition);
	assertEquals("Both cups are evaluated", 1 , testing_r2.cupPlayed);
	
	GroupRanking EightSizeGroup = tester.getBySize(teamSize+1);
	
    assertTrue(EightSizeGroup == null);
  }
  
 
}
